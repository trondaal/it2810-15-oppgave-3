var method = User.prototype;

/**
 * Model function for saving user information before sending it
 * @param username Username
 * @param password Password
 * @constructor
 */
function User(username, password){
    this._username = username;
    this._password = password;
}

method.getUsername = function(){
  return this._username;
};

method.getPassword = function(){
    return this._password;
};

module.exports = User;