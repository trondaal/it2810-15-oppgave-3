var exports = module.exports;

//This module contains possible responses from the server.

exports.INVALID_PARAMS_RESPONSE = {
    success: false,
    message: "Ugyldige parametere"
};

exports.ERROR = {
    success: false,
    message: 'Something went wrong.'
};

//--------------------------
// Authentication Responses
//--------------------------

exports.authentication = {
    NO_TOKEN : {
        success: false,
        message: 'No token'
    },

    VAL_FAILED : {
        success: false,
        message: 'Token validation failed'
    },

    AUTH_FAILED : {
        success: false,
        message: 'Authentication failed. Username or password were either incorrect or not found.'
    },

    SUCCESS : function(token){
        return {
            success: true,
            message: 'Authentication successful',
            token: token
        }
    },

    WRONG_USERNAME_OR_PASSWORD : {
        success: false,
        message: 'Username and/or password not entered'
    }
};

//--------------------------
// Signup Responses
//--------------------------

exports.signup = {
    USERNAME_IN_USE : {
        success: false,
        message: 'Username already in use'
    },

    USER_CREATED : {
        success: true,
        message: 'User created'
    },

    USER_OR_PASS_NOT_ENTERED : {
        success: false,
        message: 'Username and/or password not entered'
    }
};

//--------------------------
// Favorite Pokemon Responses
//--------------------------

exports.favorite = {
    POKEMON_SAVED_SUCCESSFULLY : {
        success: true,
        message: 'Pokemon saved'
    },

    POKEMON_DELETED : {
        success: true,
        message: 'Pokemon deleted from favorites for user'
    },

    POKEMON_ALREADY_SAVED : {
        success: true,
        message: 'Pokemon already saved for user'
    }
};