'use strict';
var exports = module.exports;

/**
 * Helper function for checking whether or not given SQL parameters are valid for the given SQL query
 * @param query SQL Query
 * @param params Parameters
 * @returns {boolean}
 */
exports.isValidParams = function(query, params){
  for (let param of params){
    if (query[param] === undefined || query[param] == "") return false;
  }
  return true
};
