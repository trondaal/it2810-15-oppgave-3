# SQL for creating pokemon table. Pokemon dataset located at: https://www.kaggle.com/abcsds/pokemon
CREATE TABLE `pokemon` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `number` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `T1` varchar(50) DEFAULT NULL,
  `T2` varchar(50) DEFAULT NULL,
  `total` varchar(50) DEFAULT NULL,
  `hp` int(11) DEFAULT NULL,
  `attack` int(11) DEFAULT NULL,
  `defense` int(11) DEFAULT NULL,
  `sp_atk` int(11) DEFAULT NULL,
  `sp_def` int(11) DEFAULT NULL,
  `speed` int(11) DEFAULT NULL,
  `generation` int(11) DEFAULT NULL,
  `legendary` varchar(10) DEFAULT NULL,
  `link` varchar(150) DEFAULT NULL,
  `evolution_chain_id` int(11) DEFAULT NULL,
  `evolves_from_number` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

# SQL for creating users table
CREATE TABLE `users` (
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`username`),
  UNIQUE KEY `users_username_uindex` (`username`)
);

# SQL for creating saved_pokemon (favorite pokemon list) table
CREATE TABLE saved_pokemon(
  user VARCHAR(50) NOT NULL,
  pokemon_id INT UNSIGNED NOT NULL,
  CONSTRAINT saved_pokemon_users_username_fk FOREIGN KEY (user) REFERENCES users (username),
  CONSTRAINT saved_pokemon_pokemon_id_fk FOREIGN KEY (pokemon_id) REFERENCES pokemon (id)
);