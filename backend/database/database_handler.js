var mysql = require('mysql');
var config = require('./database_config.js'); //Contains database configuration
var queries = require('./queries.js'); //Contains all database queries
var User = require('../models/user.js'); //Contains the user model.

var exports = module.exports;
var connection = mysql.createConnection(config.remote);

/**
 * Opens connection to the database with the specified configuration in database_config.js
 */
exports.setUp = function(){
	connection.connect();
};

/**
 * Generic Query functionality. Takes in an SQL query and queries the database,
 * sending the results to the callback function
 * @param query SQL Query
 * @param callback Callback function
 */
exports.query = function(query, callback){
	connection.query(query, function(err, rows, fields) {
  		if (err){
			callback(err.code);
		} else {
			callback(JSON.stringify(rows))
		}
	});
};


/**
 * Retrieves a user by username from the database.
 * Sends a user object to the callback function if found, null if not found.
 * @param username Username
 * @param callback Callback function
 */
exports.getUser = function(username, callback){
	connection.query(queries.GET_USER, [username], function(err, results){
		if(err){
			throw err;
		}
		if(results.length > 0){
			var user = new User(results[0].username, results[0].password);
			callback(user);
		}else{
			callback(null);
		}
	});
};

/**
 * Adds a new user to the database, sending true to the callback function if saved successfully
 * @param username Username
 * @param password Password
 * @param callback Callback function
 */
exports.saveUser = function(username, password, callback){
	connection.query(queries.SAVE_USER, [username, password], function(err){
		if(err){
			throw err;
		}
		callback(true);
	})
};

/**
 * Retrieves all the favorite Pokémon of a user, sending the results to the callback function
 * @param username User's username
 * @param callback Callback function
 */
exports.getFavoritePokemon = function(username, callback){
	connection.query(queries.GET_ALL_FAVORITE_POKEMON, [username], function(err, results){
		if(err){
			throw err;
		}
		callback(results);
	});
};

/**
 * Checks whether or not a pokemon already exists among a users saved favorites.
 * @param username Username
 * @param id Pokemon ID
 * @param callback Callback function
 */
exports.favoritePokemonCheck = function(username, id, callback){
	connection.query(queries.FAVORITE_POKEMON_CHECK, [username, id], function(err, results){
		if(err){
			throw err;
		}
		if(results.length == 0){
			callback(false);
		}else{
			callback(true);
		}
	});
};

/**
 * Adds a pokemon to a users favorite list
 * @param username Username
 * @param id Pokemon ID
 * @param callback Callback function
 */
exports.saveFavoritePokemon = function(username, id, callback){
	connection.query(queries.SAVE_FAVORITE_POKEMON, [username, id], function(err){
		if(err){
			throw err;
		}
		callback(true);
	});
};

/**
 * Removes a pokemon from a users favorite list
 * @param username Username
 * @param id Pokemon ID
 * @param callback Callback function
 */
exports.removeFavoritePokemon = function(username, id, callback){
	connection.query(queries.REMOVE_FAVORITE_POKEMON, [username, id], function(err){
		if(err){
			throw err;
		}
		callback(true);
	});
};

/**
 * Closes the connection to the database
 */
exports.end = function(){
	connection.end();
};
