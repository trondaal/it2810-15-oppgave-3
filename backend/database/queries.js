'use strict';
var exports = module.exports;
exports.GET_USER = 'SELECT * FROM users WHERE username = ?';
exports.SAVE_USER = 'INSERT INTO users (username, password) VALUES (?, ?)';

exports.SAVE_FAVORITE_POKEMON = 'INSERT INTO saved_pokemon(user, pokemon_id) VALUES (?, ?)';
exports.GET_ALL_FAVORITE_POKEMON = 'SELECT p.* FROM saved_pokemon AS sp INNER JOIN pokemon AS p ON sp.pokemon_id = p.id WHERE user = ?';
exports.REMOVE_FAVORITE_POKEMON = 'DELETE FROM saved_pokemon WHERE user = ? AND pokemon_id = ?';
exports.FAVORITE_POKEMON_CHECK = 'SELECT * FROM saved_pokemon WHERE user = ? AND pokemon_id = ?';

//Used for retrieving pokemon
exports.GET_ALL = function(query){
  let pagesize = query.pagesize;
  let offset = pagesize * (query.page -1);
  let filter = createFilter(query, "WHERE");
  let orderby = query.orderby === undefined || query.orderby === '' ? '' : 'ORDER BY ' + query.orderby;
  return (`SELECT *
            FROM pokemon
            ${filter}
            ${orderby}
            LIMIT ${offset},${pagesize} `);
};

//Used for retrieving pokemon by name
exports.QUERY_NAME = function(query){
                      let name = query.name;
                      let pagesize = query.pagesize;
                      let offset = pagesize * (query.page -1);
                      let filter = createFilter(query, "AND");
                      let orderby = query.orderby === undefined || query.orderby === '' ? '' : 'ORDER BY ' + query.orderby;
                      return (
                        `SELECT *
                        FROM pokemon
                        WHERE NAME LIKE "%${name}%"
                        ${filter}
                        ${orderby}
                        LIMIT ${offset},${pagesize}`);
                    };

//Used for retrieving pokemon by ID
exports.GET_POKEMON = function(query) {
  return `SELECT * FROM pokemon where id=${query.id}`;
};

//Used for retrieving pokemon part of an evolution chain
exports.GET_EVOLUTIONS = function(query){
  return `SELECT * FROM pokemon where evolution_chain_id=${query.evoid}`;
};

//Used to create filter, for use in GET_ALL and QUERY_NAME
let createFilter = function(query, prefix){
  let filter = [];
  if (query.t1 !== undefined){
    filter.push(`T1="${query.t1}"`)
  }
  if (query.t2 !== undefined ){
    filter.push(`T2="${query.t2}"`)
  }
  if (query.gen != undefined){
    filter.push(`generation="${query.gen}"`)
  }

  if (filter.length > 0){
    return " " + prefix + " " + filter.join(" AND ");
  }else {
    return "";
  }
};
