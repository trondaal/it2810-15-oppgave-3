var exports = module.exports;

/**
 * Local database location
 * @type {{host: string, user: string, password: string, database: string}}
 */
exports.local = {
  host     : 'localhost',
  user     : 'dev',
  password : 'dev',
  database : 'webutvikling'
};

/**
 * Remote database location
 * @type {{host: string, user: string, password: string, database: string}}
 */
exports.remote = {
  host     : 'it2810-15.idi.ntnu.no',
  user     : 'dev',
  password : 'dev',
  database : 'webutvikling'
};
