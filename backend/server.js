'use strict';

// Required packages
var express = require('express');
var bodyParser = require('body-parser'); //Used for parsing POST/GET bodies
var jwt = require('jsonwebtoken'); //Used for authentication
var bcrypt = require('bcrypt-nodejs'); //Used to encrypt passwords

var responses = require('./models/responses'); //Contains possible responses from the server.

//This is the class in charge of querying the database and returning the results
var dbHandler = require('./database/database_handler.js');

var serverHelper = require("./server_helper.js");
var queries = require('./database/queries.js');

var app = express();

//Create connection to database
dbHandler.setUp();

app.use(bodyParser.json()); //Used for parsing of responses
app.use(bodyParser.urlencoded({extended: true}));

app.set('secretVariable', 'Dette er kjempehemmelig'); //Used for signing tokens

/**
 * Tells the application to use this function for every request received, which allows for Cross Origin Requests,
 * as well as handling preflight response.
 */
app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, X-Access-Token');
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  //This needs to be handled for preflight response from browsers
  if ('OPTIONS' == req.method) {
    res.sendStatus(200);
  }else {
    next();
  }
});

//-----------------------------
// Authentication
//-----------------------------
/**
 * This route is used for authentication, and returns a valid token upon success
 */
app.post('/authenticate', function (req, res){
  var username = req.body.username;
  var password = req.body.password;

  if(username && password){
    dbHandler.getUser(username, (user) => {

      if(user && bcrypt.compareSync(password, user.getPassword())){
        var token = jwt.sign(user, app.get('secretVariable'), {
          expiresIn: 72000 //Expires in 20 hours
        });
        res.json(responses.authentication.SUCCESS(token));
      }else{
        res.json(responses.authentication.AUTH_FAILED);
      }
    });
  }else{
    res.json(responses.authentication.WRONG_USERNAME_OR_PASSWORD);
  }
});

/**
 * This route is used for creating a new user.
 */
app.post('/signup', function(req, res){
  var username = req.body.username;
  var password = req.body.password;

  if(username && password){
    var hashedPassword = bcrypt.hashSync(password);

    dbHandler.getUser(username, (user) => {
      if(user){
        res.json(responses.signup.USERNAME_IN_USE);
      }else{
        dbHandler.saveUser(username, hashedPassword, (result) => {
          if(result){
            res.json(responses.signup.USER_CREATED);
          }else{
            res.json(responses.ERROR);
          }
        })
      }
    })
  }else{
    res.json(responses.signup.USER_OR_PASS_NOT_ENTERED);
  }
});


//-----------------------------
// Routes protected by authentication
//-----------------------------

var protectedRoutes = express.Router();

/**
 * Tells the application that all routes in "appRoutes" after this function has to
 * authenticate using this function.
 */
protectedRoutes.use(function(req, res, next){
  //Check if token is included
  var token = req.body.token || req.headers['x-access-token'];

  if(token){
    jwt.verify(token, app.get('secretVariable'), function(err, decoded){
      if(err){
        res.json(responses.authentication.VAL_FAILED);
      }else{
        req.decoded = decoded;
        next();
      }
    });
  }else{
    res.status(403).json(responses.authentication.NO_TOKEN);
  }
});

/**
 * Retrieves all favorited pokemon of the logged in user
 */
protectedRoutes.get('/pokemon/', function(req, res){
  var user = req.decoded;
  dbHandler.getFavoritePokemon(user._username, response => {
    res.send(response);
  });
});

/**
 * Saves a pokemon as favorite for the logged in user
 */
protectedRoutes.post('/savePokemon/', function(req, res){
  var user = req.decoded;
  var pokemonId = req.body.pokemonid;
  if(pokemonId){
    dbHandler.favoritePokemonCheck(user._username, pokemonId, pokemon => {
      if(!pokemon){
        dbHandler.saveFavoritePokemon(user._username, pokemonId, response => {
          if(response){
            res.send(responses.favorite.POKEMON_SAVED_SUCCESSFULLY);
          }else{
            res.send(responses.ERROR);
          }
        });
      }else{
        res.send(responses.favorite.POKEMON_ALREADY_SAVED);
      }
    });
  }else{
    res.status(412);
    res.send(responses.INVALID_PARAMS_RESPONSE);
  }
});

/**
 * Removes a pokemon from favorites for the logged in user
 */
protectedRoutes.post('/removePokemon/', function(req, res){
  var user = req.decoded;
  var pokemonId = req.body.pokemonid;
  if(pokemonId){
    dbHandler.removeFavoritePokemon(user._username, pokemonId, response => {
      if(response){
        res.send(responses.favorite.POKEMON_DELETED);
      }else{
        res.send(responses.ERROR);
      }
    })
  }else{
    res.status(412);
    res.send(responses.INVALID_PARAMS_RESPONSE);
  }
});

/**
 * Sends the user profile of the logged in user.
 */
protectedRoutes.get('/', function(req, res){
  var user = req.decoded;
  res.json({
    message: 'Token authenticated',
    username: user._username,
    password: user._password
  });
});

app.use('/profile', protectedRoutes);

//-----------------------------
// Routes not protected by authentication
//-----------------------------

/**
 * Endpoint to fetch all pokemon
 */
app.get('/all/', function(req, res){
  if (serverHelper.isValidParams(req.query, ['page', 'pagesize'])){
    dbHandler.query(queries.GET_ALL(req.query), (response) => {
      res.send(response);
    })
  }else {
    res.status(412);
    res.send(responses.INVALID_PARAMS_RESPONSE);
  }
});

/**
 * Endpoint to search all pokemon by name
 * /query/?name=p&page=10&pagesize=5&orderby=name asc,number desc
 */
app.get('/query/', function(req, res){
  if (serverHelper.isValidParams(req.query, ['name', 'page', 'pagesize'])){
    dbHandler.query(queries.QUERY_NAME(req.query), (response) => {
      res.send(response);
    })
  }else {
    res.status(412);
    res.send(responses.INVALID_PARAMS_RESPONSE);
  }
});


/**
 * Endpoint to retrieve a single pokemon
 */
app.get('/single/', function(req, res){
  if (serverHelper.isValidParams(req.query, ['id'])){
    dbHandler.query(queries.GET_POKEMON(req.query), (response) => {
      res.send(response);
    })
  }else {
    res.status(412);
    res.send(responses.INVALID_PARAMS_RESPONSE);
  }
});

/**
 * Endpoint to retrieve an evolution chain
 */
app.get('/evolutions/', function(req, res){
  if (serverHelper.isValidParams(req.query, ['evoid'])){
    dbHandler.query(queries.GET_EVOLUTIONS(req.query), (response) => {
      res.send(response);
    })
  }else {
    res.status(412);
    res.send(responses.INVALID_PARAMS_RESPONSE);
  }
});


/**
 * Base
 */
app.get('/', function (req, res) {
  res.send("Server is running.");
});

/**
 * Starts the server at port 8080
 */
app.listen(8080, function () {
  console.log('[IT2810-15: Pokemon] server listening on port 8080!');
});
