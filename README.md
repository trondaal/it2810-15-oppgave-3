Oppdatert dokumentasjon ligger i "IT2810 - Gruppe 15 - Dokumentasjon.pdf"

# Struktur #

Applikasjonen er delt inn i to moduler

1. backend
2. frontend_angular

Vi har valgt å gå videre med Angular, så det er kun “backend” og “frontend_angular” som er interessant.

## Backend ##
Backendmodulen er strukturert med entry point (server.js) og prosjektkonfigurasjonen (package.json) i rotmappen, og deretter undermapper med navn som er beskrivende for hva mappen inneholder. For eksempel, så finner man for øyeblikket mappen “database”, som  inneholder alt databaserelatert. Etter hvert som vi finner det nødvendig vil nye mapper bli laget slik at det blir enkelt å finne fram til den delen av prosjektet man leter etter.

**Mappestruktur:**
```
backend
+-- database
|   +-- database_config.js
|   +-- database_handler.js
|   +-- queries.js
+-- package.json
+-- server.js

```

## Frontend ##
Frontendmodulen er strukturert med entry point (server.js), prosjektkonfigurasjon (package.json) og angularkonfigurasjon (tsconfig.js, typings.js og systemjs.config.js) i rotmappen, og deretter undermapper med navn som er beskrivende for hva mappen inneholder. Den mest relevante undermappen er app. I app-mappen ligger alt av scripts som angular bruker for å vise data på nettsiden, hvor det er en mappe for hver av de tre modultypene applikasjonen bruker: Components, Pipes og Services. Alle modulene er skrevet i TypeScript, som transpileres til JavaScript når “npm start” kjøres.

App.component er container for appen og list.component er en liste med et søkefelt. Lista blir fylt opp med pokemons via api.service når den blir initialisiert, som henter pokemons fra backend. 
Lista bruker search.pipe til å filtrere på pokemonnavn basert på teksten i søkefeltet

**Angularmoduler:**

![AngularComponents.png](https://bitbucket.org/repo/dqbyzo/images/2120713759-AngularComponents.png "Angular Components")

**Mappestruktur:**
```
frontend_angular
+-- app
|   +-- components
|       +-- app.component.ts
|       +-- list.component.ts
|   +-- pipes
|       +-- search.pipe.ts
|   +-- services
|       +-- api.service.ts
|   +-- app.module.ts
|   +-- main.ts
+-- styles
|   +-- styles.css
+-- index.html
+-- package.json
+-- systemjs.config.js
+-- tsconfig.json
+-- typings.json

```