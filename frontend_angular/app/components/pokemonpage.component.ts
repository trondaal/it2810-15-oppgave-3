import { Component, OnInit, OnDestroy } from '@angular/core';
import { APIService } from '../services/api.service';
import { ActivatedRoute } from '@angular/router';
import { PokemonListEntryComponent } from './pokemonlistentry.component';
import { UserService } from "../services/user.service";





@Component({
  selector: 'pokemon-page-component',
  styleUrls: ['styles/pokemonpage.css'],
  template: `

  <h1>{{apiService.currentPokemon.name}}</h1>,
  <div class="top-container">
    <img class="header-image" *ngIf="apiService.currentPokemon.link" src="{{apiService.currentPokemon.link}}" alt="{{apiService.currentPokemon.name}}">
    <table class="stats-table">
    <tr>
      <th>Basic stats</th>
      </tr>
      <tr>
        <td>Type 1</td>
        <td>{{apiService.currentPokemon.T1}}</td>
      </tr>
      <tr>
        <td>Type 2</td>
        <td>{{apiService.currentPokemon.T2}}</td>
      </tr>
      <tr>
        <td>HP</td>
        <td>{{apiService.currentPokemon.hp}}</td>
      </tr>
      <tr>
        <td>Attack</td>
        <td>{{apiService.currentPokemon.attack}}</td>
      </tr>
      <tr>
        <td>Defense</td>
        <td>{{apiService.currentPokemon.defense}}</td>
      </tr>
      <tr>
        <td>Special attack</td>
        <td>{{apiService.currentPokemon.sp_atk}}</td>
      </tr>
      <tr>
        <td>Special defense</td>
        <td>{{apiService.currentPokemon.sp_def}}</td>
      </tr>
      <tr>
        <td>Speed</td>
        <td>{{apiService.currentPokemon.speed}}</td>
      </tr>
      <tr>
        <td>Generation</td>
        <td>{{apiService.currentPokemon.generation}}</td>
      </tr>
    </table>
</div>
<div class="fav-container" *ngIf="userService.isLoggedIn()" >
    <div *ngIf="!userService.favoritePokemonContains(apiService.currentPokemon.id)" (click)="userService.saveFavoritePokemon(apiService.currentPokemon.id)">
      <img class="heart" src="./images/heart_black.png"/>
      <div class="text" >Legg til i favoritter</div>
    </div>
    <div *ngIf="userService.favoritePokemonContains(apiService.currentPokemon.id)" (click)="userService.removeFavoritePokemon(apiService.currentPokemon.id)">
      <img class="heart" src="./images/heart_red.png"/>
      <div class="text">Fjern fra favoritter</div>
    </div>

</div>

<div *ngIf="apiService.currentEvolutions.length > 1" >
  <div class="label-container">
    <span class="label-line left"></span>
    <span class="label">Evolutions</span>
    <span class="label-line right"></span>
  </div>
  <div class="evolutions-container">
    <div *ngFor="let pokemon of apiService.currentEvolutions" class="evolutions-entry">
      <pokemon-list-entry-component [pokemon]="pokemon"></pokemon-list-entry-component>
      <div class="arrow"></div>
    </div>
  </div>
</div>

  `
})

export class PokemonPageComponent implements OnInit, OnDestroy{
  private id = 0;
  private evoid = 0;
  private sub: any;

	  constructor(private apiService: APIService, private route: ActivatedRoute, private userService : UserService){

  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id']; // (+) converts string 'id' to a number
      this.evoid = +params['evoid'];
      this.apiService.getPokemon(this.id);
      this.apiService.getEvolutions(this.evoid);
      });
    }

  ngOnDestroy() {
   this.sub.unsubscribe();
  }
}
