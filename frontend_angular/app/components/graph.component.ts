
import {
  Component,
  OnChanges,
  AfterViewInit,
  Input,
  ElementRef,
  ViewChild
} from '@angular/core';

import { Router } from '@angular/router';
import { APIService } from '../services/api.service';
import { AppRoutingModule } from '../app-routing.module';


@Component({
  selector: 'barchart2',
  templateUrl: 'html/drawChart.html',
  styleUrls: ['styles/graph.css'],
})

export class GraphComponent  {

  //All variables with @Input are to be used in the html
  @Input() x: number = 0;
  @Input() y: number = 0;
  @Input() text = "";
  @Input() Twidth: number = 120;
  @Input() Theight = 40;
  @Input() hide = true;
  @ViewChild('canvas') canvasRef:ElementRef;
  @ViewChild('div') divRef:ElementRef;
  private canvas: any;
  private div: any;
  @Input() data: number[];
  @Input() width: number;
  @Input() height: number;
  @Input() colors: string[];
  @Input() title: string;
  public linkData: any[] = [];

  //Setters and getters for the info box

  public setText(text){
    this.text = text;
  }
  public getText(){
    return this.text;
  }
  public setHidden(b){
    this.hide = b;
  }

  public setX(x){
    this.x = x;
  }
  public setY(y){
    this.y = y;
  }
  public getX(){
    return this.x;
  }
  public getY(){
    return this.y;
  }

	constructor(private router: Router){
    //console.log("BarchartComponent constructor: width: ", this.width, ", height: ", this.height, ", colors: ", this.colors, ' this.canvas: ', this.canvas);
  }
  ngAfterViewInit() {
    //Init the canvas
    //console.log("BarchartComponent ngAfterViewInit: width: ", this.width, ", height: ", this.height, ", colors: ", this.colors, ' this.canvas: ', this.canvas);
    this.canvas = this.canvasRef.nativeElement;
    this.div = this.divRef.nativeElement;
    this.canvas.width = this.width;
    this.canvas.height = this.height;
    this.draw();
    const c = this.canvas;
    let linkData = this.linkData;
    let active = false;
    let oX = 0;
    let oY = 0;
    let sX = 0;
    let sY = 0;
    let currColor = 0;
    const ctx = c.getContext('2d');
    //console.log(this.getText());
    //console.log(this.getX());

    // To use this inside eventListener
    let domain = this;

    //Mouseevent on canvas isnt called if the div is in the way, so add a mouse event to div aswell 
    this.div.addEventListener("mousemove", function(event: MouseEvent){
      domain.setHidden(true);
    })

    //Mouseevent for clicks on bars
    c.addEventListener("click", function(event: MouseEvent){

      let rect = c.getBoundingClientRect();

      let x = event.clientX - rect.left;
      let y = event.clientY - rect.top;

      for(let i = 0, l = linkData.length; i < l; i++) {
        if((x > linkData[i][0] && x < linkData[i][2]) && (y > linkData[i][1] && y < linkData[i][3])){                         
          let type = linkData[i][5];
          domain.router.navigate(['/list', type.toLowerCase()]);
        }
      }
    })

    //Checks if mouse moves on the bars and launches infobox / highlight
    c.addEventListener("mousemove", function(event: MouseEvent){

      let rect = c.getBoundingClientRect();

      let x = event.clientX - rect.left;
      let y = event.clientY - rect.top;


      for(let i = 0, l = linkData.length; i < l; i++) {

        if((x > linkData[i][0] && x < linkData[i][2]) && (y > linkData[i][1] && y < linkData[i][3])){

          if(active){

            //Init and show infobox
            domain.setText("Type: " + linkData[i][5] + "\n" + "Number: " + linkData[i][4]);
            domain.setHidden(false);
            domain.setX(event.pageX + 5);
            domain.setY(event.pageY - 40);
            break;
          }
            active = true;
            //Highlight here
            ctx.globalAlpha = 0.2;
            ctx.fillStyle = 'white';
            ctx.fillRect(linkData[i][0], linkData[i][1], 30, linkData[i][3] - linkData[i][1]);
            ctx.fillStyle = 'black';
            ctx.rect(linkData[i][0], linkData[i][1], 30, linkData[i][3] - linkData[i][1]);
            ctx.stroke();

            oX = linkData[i][0];
            oY = linkData[i][1];
            sX = 30;
            sY = linkData[i][3] - linkData[i][1];
            currColor = linkData[i][6];


            break;
          }
        else if(i == (l - 1)){

          //Hide infobox
          domain.setHidden(true);
          active = false;

          ctx.globalAlpha = 1;
          ctx.fillStyle = currColor;
          ctx.fillRect(oX, oY, sX, sY);
          ctx.fillStyle = 'black';
          ctx.rect(oX, oY, sX, sY);
          ctx.stroke();
        }
      }
    })
  }
  //Draw the static part of the chart
  draw() {
    if(this.canvas.getContext) {
      const c = this.canvas.getContext('2d');
      const linkData = this.linkData;

      //Fill in the background
      c.fillStyle = "#F6F6F6";
      c.fillRect(0,0,this.width,this.height);

      const xOffset = 25;

      //Draw data bars
      let values = this.data.map( (dp) => {
        return dp['value'];
      });
      const labels = this.data.map( (dp) => {
        return dp['label'];
      });

      for(let i = 0, l = values.length, j = this.colors.length; i < l; i++) {

        let currData: any[] = [];

        c.fillStyle = this.colors[i];
        let dp = values[i];

        c.fillRect(xOffset + i*(this.width)/(l+1) + 5, this.height-40-dp*2.5, 30, dp*2.5);
        c.fillStyle = 'black';
        c.rect(xOffset + i*(this.width)/(l+1) + 5, this.height-40-dp*2.5, 30, dp*2.5);
        c.stroke();

        //Create the info array to be used for highlighting and infobar
        let startX = xOffset + i*(this.width)/(l+1) + 5;
        let startY = this.height-40-dp*2.5;
        let endX = 30;
        let endY = dp*2.5;

        currData.push(startX);
        currData.push(startY);
        currData.push(startX + endX);
        currData.push(startY + endY);
        currData.push(values[i]);
        currData.push(labels[i]);
        currData.push(this.colors[i]);

        linkData.push(currData);
      }

      //Draw axis lines
      c.fillStyle = 'black';
      c.beginPath();
      c.moveTo(xOffset+5, 40);
      c.lineTo(xOffset+5, this.height - 40);
      c.lineTo(this.width, this.height - 40);
      c.stroke();

      //Draw text and vertical lines (value label and tick mark down left side
      c.fillStyle = 'black';
      let limit = this.height / 50 - 1;
      for(let i = 0; i < limit; i++) {
        let yTickOffset = (this.height - 40) - i*50;
        c.fillText(i*20 + "", 4, yTickOffset);
        c.beginPath();
        c.moveTo(xOffset, yTickOffset);
        c.lineTo(xOffset+5, yTickOffset);
        c.stroke();
      }

      //Finally, add labels for the x-axis
      //Draw horiz text
      for(let i = 0, l = labels.length; i < l; i++) {
        c.fillText(labels[i],30 + i*this.width/(l+1), this.height - 25);
      }
    }
  }
}