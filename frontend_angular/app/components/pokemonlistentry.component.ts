import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from "../services/user.service";


@Component({
  selector: 'pokemon-list-entry-component',
  styleUrls: ['styles/pokemonlistentrycomponent.css'],
  template: `
              <div *ngIf="userService.isLoggedIn()" class="heart-container">
                  <!---<img *ngIf="!userService.favoritePokemonContains(pokemon.id)" (click)="userService.saveFavoritePokemon(pokemon.id)" class="heart" src="./images/heart_black.png"/>-->
                  <img *ngIf="userService.favoritePokemonContains(pokemon.id)"  class="heart" src="./images/heart_red.png"/>
              </div>
              <div class="imageholder" (click)="view(pokemon.id, pokemon.evolution_chain_id)">
                  <img *ngIf="pokemon.link" src="{{pokemon.link}}" alt="{{pokemon.name}}">
              </div>
              <div class="listentry-number">
                #{{pokemon.number}}
              </div>
              <div class="listentry-name">
                  {{pokemon.name}}
              </div>
              <div class="listentry-types">
                {{pokemon.T1 | uppercase}} {{pokemon.T2 | uppercase}}
              </div>



            `,
})
export class PokemonListEntryComponent{
  @Input() pokemon: Object;

  constructor(private router:Router, private userService : UserService){}

  view(id: number, evoid: number){
    this.router.navigate([`/view`, id, evoid]);
  }


}
