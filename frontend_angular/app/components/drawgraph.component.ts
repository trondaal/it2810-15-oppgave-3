import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { APIService } from '../services/api.service';


@Component({
    selector: 'drawGraphComponent',
    templateUrl: 'html/chart.html',
})
export class DrawGraphComponent {

    consistencyData = [

        { label: 'Fire', value: 64 },
        { label: 'Grass', value: 95 },
        { label: 'Normal', value: 102 },
        { label: 'Fighting', value: 53 },
        { label: 'Flying', value: 101 },
        { label: 'Poison', value: 62 },
        { label: 'Ground', value: 67 },
        { label: 'Rock', value: 58 },
        { label: 'Bug', value: 72 },
        { label: 'Ghost', value: 46 },
        { label: 'Steel', value: 49 },
        { label: 'Water', value: 126 },
        { label: 'Electric', value: 50 },
        { label: 'Psychic', value: 90 },
        { label: 'Ice', value: 38 },
        { label: 'Dragon', value: 50 },
        { label: 'Dark', value: 51 },
        { label: 'Fairy', value: 40 },
    ] ;
    colors2 = [ '#F08030', '#78C850', '#7038F8', '#A8A878','#C03028', 
    '#A890F0', '#E0C068', '#B8A038', '#A8B820', '#705898', '#B8B8D0',
    '#6890F0', '#F8D030', '#F85888', '#98D8D8', '#7038F8', '#705848', 
    '#FFAEC9' ];

    getRandomInt(max = 100) {
        return Math.floor(Math.random()*max);
    }
}