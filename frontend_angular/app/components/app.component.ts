import { Component } from '@angular/core';
import {UserService} from "../services/user.service";

/*
Application components. Contains all other components.
 */
@Component({
  selector: 'my-app',

  template: `
  <div class="navbar">
  <a routerLink="/list" class="title" routerLinkActive="active">Pokemon Databasen</a>
    <div class="navlinks">
      <a routerLink="/user" routerLinkActive="active">Favoritter</a>
      <a routerLink="/graph" routerLinkActive="active">Type-diagram</a>
    </div>
    <div class="navuser">
      <div class="username" *ngIf="userService.isLoggedIn()">Logget inn som <span class="text">{{userService.username}}</span></div>
      <div *ngIf="userService.isLoggedIn()" class="logout-button" (click)="userService.logout()">Logg ut</div>
      <a *ngIf="!userService.isLoggedIn()" class="logout-button" routerLink="/user" routerLinkActive="active">Logg inn</a>
    </div>
  </div>
    <router-outlet></router-outlet>
  `
})
export class AppComponent {
  constructor(private userService : UserService){
    userService.initialize();
  }
}
