import {Component, OnInit} from '@angular/core';
import {UserService} from "../services/user.service";
import {PokemonListEntryComponent } from './pokemonlistentry.component'



@Component({
  selector: 'user-component',
  styleUrls: ['styles/usercomponent.css'],
  template: `
  <div class="container">
  <h1>
    <div *ngIf="userService.isLoggedIn()">Dine favoritter</div>
    <div *ngIf="userService.isLoggedIn() === false">Velkommen, Gjest</div>
  </h1>
  <div *ngIf="userService.isLoggedIn() === false" class="loggedIn">
  	<div class="message">
    Du er ikke logget inn.<br>
    Logg inn eller lag en bruker.
    </div>
	  <div class="message"> NB! Vi har ikke implementert noen feilmeldinger eller validering av input, så om det ikke skjer noe har du enten brukt et brukernavn som allerede eksisterer,
  	eller så har du skrevet feil brukernavn og passord.</div>


      <div class="form">
      <form id="loginForm">
          <input type="text" name="username" [(ngModel)]="form_username" placeholder="Brukernavn" class="textfield" autocomplete="off">
          <input type="password" name="password" [(ngModel)]="form_password" placeholder="Passord" class="textfield" autocomplete="off">
          <div class="submit-container">
            <input type="submit" name="login" value="Logg inn"  class="button login" (click)="loginForm(true)">
            <input type="submit" name="signup" value="Registrer" class="button signup" (click)="loginForm(false)">
          </div>
      </form>
      <p *ngIf="missing == true">Mangler brukernavn og/eller passord</p>
      </div>

      <div class="testuser">
        Testbruker:
        <span> Brukernavn: test </span>
        <span> Passord: passord </span>
      </div>
  </div>


  <div *ngIf="userService.isLoggedIn()">
    <div class="saved-pokemon-container">
      <div  *ngFor="let pokemon of userService.favoritePokemon" class="saved-pokemon-entry">
        <pokemon-list-entry-component [pokemon]="pokemon"></pokemon-list-entry-component>
        <button (click)="userService.removeFavoritePokemon(pokemon.id)">Fjern fra favoritter</button>
      </div>
    </div>
  </div>
  </div>

  `
})

export class UserComponent implements OnInit{

    form_username;
    form_password;
    missing = false;

    constructor(private userService : UserService){
    }

    ngOnInit(): void {
        if(this.userService.isLoggedIn()){
            this.userService.initialize();
        }
    }

    /**
     * Retrieves the parameters from the form, and submits them to the user service for logging in or signing up
     * @param isLoggingIn
     */
    loginForm(isLoggingIn){
        var username = this.form_username;
        var password = this.form_password;

        //HER TRENGS DET VALIDERING
        if(username && password){
            if(isLoggingIn){
                this.userService.login(username, password);
                this.missing = false;
            }else{
                this.userService.signUp(username, password);
                this.missing = false;
            }

        }else{
            this.missing = true;
        }
    }
}
