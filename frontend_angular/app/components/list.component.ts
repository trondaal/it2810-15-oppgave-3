import { Component, Input, OnDestroy  } from '@angular/core';
import { OnInit } from '@angular/core';
import { APIService } from '../services/api.service';
import { PokemonListEntryComponent } from './pokemonlistentry.component';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'list-component',
  styleUrls: ['styles/listcomponent.css'],
  template: `

          <searchbar-component></searchbar-component>
          <div class="sortbar-top">
            <span class="sortbar-line left"></span>
            <span class="sortlabel"> Sorter</span>
            <span class="sortbar-line right"></span>
          </div>
            <div class="sortbar">

              <div>
                  <a (click)="toggleSort('number')">#</a>
                  <span *ngIf="apiService.query.sorting.number == 'ASC'" class="ion-arrow-up-b"></span>
                  <span *ngIf="apiService.query.sorting.number == 'DESC'" class="ion-arrow-down-b"></span>
              </div>
              <div>
                <a (click)="toggleSort('name')">Navn</a>
                <span *ngIf="apiService.query.sorting.name == 'ASC'" class="ion-arrow-up-b"></span>
                <span *ngIf="apiService.query.sorting.name == 'DESC'" class="ion-arrow-down-b"></span>
              </div>
              <div>
                <a (click)="toggleSort('t1')">Type 1</a>
                <span *ngIf="apiService.query.sorting.t1 == 'ASC'" class="ion-arrow-up-b"></span>
                <span *ngIf="apiService.query.sorting.t1 == 'DESC'" class="ion-arrow-down-b"></span>
              </div>
              <div>
                <a (click)="toggleSort('t2')">Type 2</a>
                <span *ngIf="apiService.query.sorting.t2 == 'ASC'" class="ion-arrow-up-b"></span>
                <span *ngIf="apiService.query.sorting.t2 == 'DESC'" class="ion-arrow-down-b"></span>
              </div>
              <div>
                <a (click)="toggleSort('generation')">Generasjon</a>
                <span *ngIf="apiService.query.sorting.generation == 'ASC'" class="ion-arrow-up-b"></span>
                <span *ngIf="apiService.query.sorting.generation == 'DESC'" class="ion-arrow-down-b"></span>
              </div>
            </div>
            <span class="navigation">
                <button class="ion-chevron-left"(click)="navigate(-1)"></button>
                {{apiService.query.page}}
                <button class="ion-chevron-right"(click)="navigate(1)"></button>
            </span>
            <div class="cardview">
              <pokemon-list-entry-component *ngFor="let pokemon of apiService.pokemon" [pokemon]="pokemon"></pokemon-list-entry-component>
            </div>
            <span class="navigation">
                <button class="ion-chevron-left"(click)="navigate(-1)"></button>
                {{apiService.query.page}}
                <button class="ion-chevron-right"(click)="navigate(1)"></button>
              </span>
          `,
})

/*
The function of this class is to show a list of Pokemon
 */
export class ListComponent implements OnInit{
  constructor(private apiService: APIService,  private route: ActivatedRoute){

  }
  private sub: any;


  ngOnInit(): void {
    this.sub = this.route.params.subscribe(params => {
      let type = params['type'];
      if (type){
        this.apiService.query.filter.type1 = type;
      }
      });

    this.apiService.doQuery();

  }


  ngOnDestroy() {
   this.sub.unsubscribe();
  }

  navigate(direction: number): void{
    this.apiService.query.page += direction;
    if (this.apiService.query.page < 1){
      this.apiService.query.page = 1;
    } else {
      this.apiService.doQuery()
    }
  }

  toggleSort(sortOn: string): void{
    let sorting = this.apiService.query.sorting;
    if (sorting[sortOn] == undefined){
      sorting[sortOn] = "ASC"
    } else if(sorting[sortOn] == "ASC"){
      sorting[sortOn] = "DESC"
    } else {
      delete sorting[sortOn];
    }
    this.apiService.doQuery()
  }

}
