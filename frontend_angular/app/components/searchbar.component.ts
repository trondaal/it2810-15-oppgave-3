import { Component, Input } from '@angular/core';
import { APIService } from '../services/api.service';

@Component({
  selector: 'searchbar-component',
  styleUrls: ['styles/searchbarcomponent.css'],
  template: `
                <div class="search-box">
                  <input class="search-bar" [(ngModel)]="apiService.query.name" (keyUp.enter)="newQuery()" placeholder="Søk etter pokemon..." autofocus>
                </div>
                <div class="filter-box">
                  <select class="filter-select type1"[(ngModel)]="apiService.query.filter.type1"  (ngModelChange)="newQuery()">
                    <option value="-1">TYPE 1</option>
                    <option *ngFor="let type of typeFilterOptions" [ngValue]="type">{{type | uppercase}}</option>
                  </select>
                  <select class="filter-select type2" [(ngModel)]="apiService.query.filter.type2" (ngModelChange)="newQuery()">
                    <option value="-1">TYPE 2</option>
                    <option *ngFor="let type of typeFilterOptions" [ngValue]="type">{{type | uppercase}}</option>
                  </select>
                  <select class="filter-select generation"[(ngModel)]="apiService.query.filter.generation" (ngModelChange)="newQuery()">
                    <option value="-1">GENERASJON</option>
                    <option *ngFor="let generation of generationFilterOptions" [ngValue]="generation">{{generation}}</option>
                  </select>
                </div>
            `,
})
export class SearchBarComponent{
  @Input() test: string;
  @Input() pokemon: Array<Object>;
  page = 1;
  pageSize = 25;

  generationFilterOptions= [1, 2, 3, 4, 5, 6];
  typeFilterOptions = ["normal", "fighting", "flying", "poison", "ground", "rock",
                        "bug", "ghost", "steel", "fire", "water", "grass", "electric",
                        "psychic", "ice", "dragon", "dark", "fairy"];


  constructor(private apiService: APIService){}

  newQuery(): void{
    this.apiService.query.page = 1;
    this.apiService.doQuery()
  }

}
