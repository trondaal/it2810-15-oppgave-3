import { NgModule }     from '@angular/core';
import { RouterModule } from '@angular/router';

import { ListComponent } from './components/list.component';
import { UserComponent } from './components/user.component';
import { DrawGraphComponent } from './components/drawgraph.component';
import { PokemonPageComponent } from './components/pokemonpage.component';

@NgModule({
  imports: [
    RouterModule.forRoot([
      { path: '', component: ListComponent},
      { path: 'list', component: ListComponent },
      { path: 'list/:type', component: ListComponent },
      { path: 'user', component: UserComponent },
      { path: 'graph', component: DrawGraphComponent },
      { path: 'view/:id/:evoid', component: PokemonPageComponent}
    ])
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {}
