import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { HttpModule }    from '@angular/http';
import { AppRoutingModule } from './app-routing.module';

// Components
import { AppComponent }  from './components/app.component';
import { ListComponent } from './components/list.component';
import { UserComponent } from './components/user.component';
import { GraphComponent } from './components/graph.component';
import { DrawGraphComponent } from './components/drawgraph.component';
import { PokemonListEntryComponent } from './components/pokemonlistentry.component';
import { SearchBarComponent } from './components/searchbar.component'
import { APIService } from './services/api.service';
import { UserService } from "./services/user.service";
import { PokemonPageComponent } from './components/pokemonpage.component';



// Defines which modules, components and pipes that are to be used
@NgModule({
  imports:      [
    BrowserModule,
    HttpModule,
    FormsModule,
    AppRoutingModule,
  ],
  declarations: [
    AppComponent,
    ListComponent,
    UserComponent,
    GraphComponent,
    DrawGraphComponent,
    SearchBarComponent,
    PokemonListEntryComponent,
    PokemonPageComponent
  ],
  providers: [APIService, UserService],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
