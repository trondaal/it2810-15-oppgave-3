import { Injectable } from '@angular/core';
import {Http, Headers, RequestOptions} from '@angular/http';
import 'rxjs/add/operator/toPromise';


/**
 * This service is used for everything related to user management
 */
@Injectable()
export class UserService {
    private API_BASE = 'http://localhost:8080';
    private PROFILE_BASE = this.API_BASE + '/profile';

    private loggedIn = false;
    private username;
    private password;

    public favoritePokemon = [];
    private favoritePokemonIDs = [];

    constructor(private http: Http){
        //localStorage.getItem returns "null" if item does not exist.
        this.loggedIn = localStorage.getItem('token') !== null;
    }

    /**
     * Initializes information
     */
    initialize(){
        if(this.loggedIn){
            this.getFavoritePokemon();
            this.getProfile();
        }
    }

    getUsername(){
        return this.username;
    }

    getPassword(){
        return this.password;
    }

    /**
     * Sends login information to server, and if authorization is successful adds token to Local Storage.
     * @param username Username used for authentication
     * @param password Password used for authentication
     */
    login(username, password) {
        var endpoint = this.API_BASE + "/authenticate";
        var body = {
            'username': username,
            'password': password
        };

        return this.http.post(endpoint, body)
            .toPromise()
            .then(response => response.json())
            .then(response => {
                if(response.success){
                    localStorage.setItem('token', response.token);
                    this.getFavoritePokemon();
                    this.getProfile();
                    this.loggedIn = true;
                }
            });
    }

    /**
     * Logs out. Removes token from Local Storage.
     */
    logout() {
        localStorage.removeItem('token');
        this.loggedIn = false;
    }

    /**
     * Returns true if used is logged in, false if user isn't logged in
     * @returns {boolean}
     */
    isLoggedIn() {
        return this.loggedIn;
    }

    /**
     * This function is used to create a new user.
     * @param username
     * @param password
     * @returns {boolean} true if successful, false if not
     */
    signUp(username, password) {
        var endpoint = this.API_BASE + "/signup";
        var body = {
            'username': username,
            'password': password
        };

        return this.http.post(endpoint, body)
            .toPromise()
            .then(response => response.json());
    }

    //-------------------------------------
    // These functions require the use of the authentication
    // header to receive response from backend
    //-------------------------------------

    /**
     * Helper function to generate a header that contains token, for authentication
     * @returns {RequestOptions}
     */
    private createAuthenticationHeader(){
        let token = localStorage.getItem('token');
        let headers = new Headers({
            'Accept': 'application/json',
        });
        headers.append('x-access-token', token);

        return new RequestOptions({headers: headers});
    }

    /**
     * Retrieves the user profile from the server.
     */
    private getProfile(){
        let options = this.createAuthenticationHeader();

        return this.http.get(this.PROFILE_BASE, options)
            .toPromise()
            .then(response => response.json())
            .then(user => {
                this.username = user.username;
                this.password = user.password;
            });
    }

    /**
     * Retrieves the users favorite pokemon from the server
     * @returns {Promise<TResult>}
     */
    private getFavoritePokemon(){
        let options = this.createAuthenticationHeader();
        this.favoritePokemonIDs = [];

        return this.http.get(this.PROFILE_BASE + "/pokemon/", options)
            .toPromise()
            .then(response => response.json())
            .then(pokemon => {
                this.favoritePokemon = pokemon;
                for(let i = 0; i < pokemon.length; i++){
                    this.favoritePokemonIDs.push(pokemon[i].id);
                }
            });
    }

    /**
     * Adds a new pokemon to the favorite pokemon list
     * @param id Pokemon (Database) id
     * @returns {Promise<TResult>}
     */
    saveFavoritePokemon(id){
        if(this.loggedIn){
            let options = this.createAuthenticationHeader();
            let body = {
              pokemonid: id
            };

            //Used to get responsive buttons in the pokemon lists
            if(this.favoritePokemonIDs.indexOf(id) === -1){
                this.favoritePokemonIDs.push(id);
            }

            return this.http.post(this.PROFILE_BASE + "/savePokemon/", body, options)
                .toPromise()
                .then(response => response.json());
        }
    }

    /**
     * Removes a pokemon from the favorite pokemon list
     * @param id Pokemon (Database) id
     * @returns {Promise<TResult>}
     */
    removeFavoritePokemon(id){
        if(this.loggedIn){
            let options = this.createAuthenticationHeader();
            let body = {
                pokemonid: id
            };
            //Used to get responsive buttons in the pokemon lists
            var indexOfId = this.favoritePokemonIDs.indexOf(id);
            if(indexOfId !== -1){
                this.favoritePokemonIDs.splice(indexOfId, 1);
            }

            for(let i = 0; i < this.favoritePokemon.length; i++){
                if(this.favoritePokemon[i].id === id){
                    this.favoritePokemon.splice(i,1);
                    break;
                }
            }

            return this.http.post(this.PROFILE_BASE + "/removePokemon/", body, options)
                .toPromise()
                .then(response => response.json());
        }
    }

    /**
     * Helper function to check whether or not a given pokemon is in the favorites list
     * @param id
     * @returns {boolean}
     */
    favoritePokemonContains(id){
        if(this.loggedIn){
            if(this.favoritePokemonIDs.indexOf(id) !== -1){
                return true;
            }
        }
        return false;
    }

}
