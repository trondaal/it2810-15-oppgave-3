import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import 'rxjs/add/operator/toPromise';

/*
  The function of this class is to retrieve data & information from the backend server.
 */
@Injectable()
export class APIService {
  private API_BASE = 'http://localhost:8080';
  public pokemon = [];
  public currentPokemon = "";
  public currentEvolutions = [];
  public query = {
    name: "",
    page: 1,
    pagesize: 20,
    filter: {
      type1: "-1",
      type2: "-1",
      generation: "-1"
    },
    sorting: {

    }
  };

  constructor(private http: Http){

  }

  getEvolutions(evoid: number){
    let queryUrl = `/evolutions?evoid=${evoid}`
    return this.http.get(this.API_BASE + queryUrl)
                .toPromise()
                .then(response => response.json())
                .then((evolutions) => {
                  this.currentEvolutions = evolutions;
                })
  }


  getPokemon(id: number){
    let queryUrl = `/single?id=${id}`
    return this.http.get(this.API_BASE + queryUrl)
                .toPromise()
                .then(response => response.json())
                .then((pokemon) => {
                  this.currentPokemon = pokemon[0];
                })
  }

  doQuery(){
    let queryUrl = this.constructQuery();
    return this.http.get(this.API_BASE + queryUrl)
                .toPromise()
                .then(response => response.json())
                .then((pokemon) => {
                  this.pokemon = pokemon
                })
  }

  private constructQuery(){
    //?name=${name}&page=${page}&pagesize=${pagesize}`;
    let base = '';
    let name = '';
    let filter = '';
    let orderby = this.createOrderby(this.query.sorting);
    if (this.query.name){
      base = 'query';
      name = `name=${this.query.name}&`
    } else {
      base = 'all'
    }

    if (this.query.filter.type1 !== "-1"){
      filter += `&t1=${this.query.filter.type1}`
    }

    if (this.query.filter.type2 !== "-1"){
      filter += `&t2=${this.query.filter.type2}`
    }

    if (this.query.filter.generation !== "-1"){
      filter += `&gen=${this.query.filter.generation}`
    }
    let query = `/${base}/?${name}page=${this.query.page}&pagesize=${this.query.pagesize}${filter}${orderby}`;
    console.log(query);
    return query;
  }

  private createOrderby(elements){
    let out = [];
    let keys = Object.keys(elements);
    for (let i = 0; i < keys.length; i++){
      out.push(`${keys[i]} ${elements[keys[i]]}`);
    }
    if (keys.length == 0){
      return "";
    }
    return "&orderby=" + out.join(", ");
  }

}
