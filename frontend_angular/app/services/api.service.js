"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
require('rxjs/add/operator/toPromise');
/*
  The function of this class is to retrieve data & information from the backend server.
 */
var APIService = (function () {
    function APIService(http) {
        this.http = http;
        this.API_BASE = 'http://localhost:8080';
        this.pokemon = [];
        this.currentPokemon = "";
        this.currentEvolutions = [];
        this.query = {
            name: "",
            page: 1,
            pagesize: 20,
            filter: {
                type1: "-1",
                type2: "-1",
                generation: "-1"
            },
            sorting: {}
        };
    }
    APIService.prototype.getEvolutions = function (evoid) {
        var _this = this;
        var queryUrl = "/evolutions?evoid=" + evoid;
        return this.http.get(this.API_BASE + queryUrl)
            .toPromise()
            .then(function (response) { return response.json(); })
            .then(function (evolutions) {
            _this.currentEvolutions = evolutions;
        });
    };
    APIService.prototype.getPokemon = function (id) {
        var _this = this;
        var queryUrl = "/single?id=" + id;
        return this.http.get(this.API_BASE + queryUrl)
            .toPromise()
            .then(function (response) { return response.json(); })
            .then(function (pokemon) {
            _this.currentPokemon = pokemon[0];
        });
    };
    APIService.prototype.doQuery = function () {
        var _this = this;
        var queryUrl = this.constructQuery();
        return this.http.get(this.API_BASE + queryUrl)
            .toPromise()
            .then(function (response) { return response.json(); })
            .then(function (pokemon) {
            _this.pokemon = pokemon;
        });
    };
    APIService.prototype.constructQuery = function () {
        //?name=${name}&page=${page}&pagesize=${pagesize}`;
        var base = '';
        var name = '';
        var filter = '';
        var orderby = this.createOrderby(this.query.sorting);
        if (this.query.name) {
            base = 'query';
            name = "name=" + this.query.name + "&";
        }
        else {
            base = 'all';
        }
        if (this.query.filter.type1 !== "-1") {
            filter += "&t1=" + this.query.filter.type1;
        }
        if (this.query.filter.type2 !== "-1") {
            filter += "&t2=" + this.query.filter.type2;
        }
        if (this.query.filter.generation !== "-1") {
            filter += "&gen=" + this.query.filter.generation;
        }
        var query = "/" + base + "/?" + name + "page=" + this.query.page + "&pagesize=" + this.query.pagesize + filter + orderby;
        console.log(query);
        return query;
    };
    APIService.prototype.createOrderby = function (elements) {
        var out = [];
        var keys = Object.keys(elements);
        for (var i = 0; i < keys.length; i++) {
            out.push(keys[i] + " " + elements[keys[i]]);
        }
        if (keys.length == 0) {
            return "";
        }
        return "&orderby=" + out.join(", ");
    };
    APIService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], APIService);
    return APIService;
}());
exports.APIService = APIService;
//# sourceMappingURL=api.service.js.map